#!/bin/bash

# Get the date of the latest commit in YYYYMMDD format
COMMIT_DATE=$(git log -1 --format=%cd --date=format:%y%m%d)

# Get the first eight digits of the latest commit hash
COMMIT_HASH=$(git rev-parse --short=8 HEAD)

# Combine date and commit hash to form the new version
NEW_VERSION="${COMMIT_DATE}-${COMMIT_HASH}"


# Set the new version
mv target/*.jar target/${NEW_VERSION}.jar

echo ${NEW_VERSION}


