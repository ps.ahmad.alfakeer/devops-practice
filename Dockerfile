FROM openjdk:8-jdk-alpine

WORKDIR /app

# Declare the build argument
ARG NEW_VERSION

# Copy the JAR file from the build context to the image
COPY target/${NEW_VERSION}.jar /app/my-app.jar 

#ENV SPRING_PROFILES_ACTIVE=Mysql
ENV JAVA_OPTS="-Xms256m -Xmx512m"

# Set the command to run the application
CMD [ "sh", "-c", "java ${JAVA_OPTS} -jar /app/my-app.jar"]
